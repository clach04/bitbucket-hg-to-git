# BitBucket Hg to Git
Ed Randall, 2/11/19

Following the recent announcement that BitBucket will be 
['sunsetting' support for Mercurial repositories from 1/6/20](https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket) 
it became fairly urgent for me to find a straightforward solution to migrate my Hg projects over to Git.

I only have a few projects but an automated approach should be less error-prone and able to preserve 
project metadata where possible. I therefore created this simple tool to handle my migration needs.

This project is a simple Python 3.7 script that makes use of the BitBucket 2.0 API to identify and 
create new equivalent Git repositories and a Git/Mercurial compatibility addon to do the conversion.


I've used it only in a Cygwin environment on Windows but I tried to keep it fairly generic so hopefully 
it will work anywhere that Python 3 is installed.  I used Python 3.7.4.

It functions approximately as follows:

```
1) Ensure git and git-remote-hg are available on PATH
2) Connect (authenticate) to BitBucket API
3) Read list of projects for user
4) For each project:
5)    If it's not a Mercurial project, skip processing
6)    If a Git equivalent project exists already, skip processing
7)    Create Git equivalent project on BitBucket
8)    Clone project locally using git clone --mirror with hg:: prefix
9)    Push local project to new git remote using git push --mirror
```

For the authentication (2) you should set up a temporary App Password 
(under your account > Settings > Access Management > App Passwords) with 'Admin' access to your repositories.  
You can easily revoke this when done.  I used the following configuration:

The script can be found in a Git project [bitbucket-hg-to-git](https://bitbucket.org/edrandall/bitbucket-hg-to-git) 
and is dedicated to the community. Please feel free to modify it for your own needs or even raise issues 
and pull-requests if necessary.


