#!/usr/bin/env python3
# Convert Mercurial projects on bitbucket.org to Git
# Uses the bridge: https://github.com/felipec/git-remote-hg
#

import argparse
import datetime
import os
import sys
import re
import traceback
import base64
import json
import urllib.parse, urllib.request
from pathlib import Path
from contextlib import contextmanager

#----------------------------------------
# Encapsulates the BitBicket API to handle authentication and provide higher-level access methods.
# For API @see  https://developer.atlassian.com/bitbucket/api/2/reference/resource/
# For partial-response field selection @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/partial-response
# For filtering and sorting parameters @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/filtering
# For paginatiopn parameters @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/pagination 
# Authentications uses 'App Passwords' @see https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html
# also @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/authentication
#
class BBRestApi:
	def __init__(self, apiBaseUrl, **authArgs):
		self.baseUrl = apiBaseUrl
		self.username = authArgs.get('username')
		encodedAuth = base64.b64encode('{username}:{secret}'.format(**authArgs).encode()).decode()
		self.authHeader = ( 'Authorization', 'Basic {}'.format(encodedAuth) ) 
		self.jsonContentType = ( 'Content-Type', 'application/json' ) 


	#----------------------------------------
	# Fetch all repositories for username.
	# Iterates through each page of results to collect all respositories.  
	def getAllRepositories(self, queryParams):
		lastPage = False
		nextUrl = None
		totalSize = 0
		totalRead = 0
		allRepositories = []
		
		while not lastPage:
			if not nextUrl:
				data = self.getRepositoriesPaged(queryParams)
			else:
				data = self.getURL(nextUrl)
			nextUrl = data.get('next', None)
			lastPage = (nextUrl == None)
			values = data.get('values')
			if not values:
				log('No values returned.')
				break
			allRepositories.extend(values)
			totalRead += len(values)
			totalSize = data.get('size', 0)
			page = data.get('page')
			log('Read {}/{} results beginning at page {}; lastPage={}'.format(totalRead, totalSize, page, lastPage))
			
		return allRepositories
		
	#----------------------------------------
	# Fetch repositories for username.
	# @see https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D
	# For pagination @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/pagination
	# For query filterinf @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/filtering
	def getRepositoriesPaged(self, queryParams):
		uri = '2.0/repositories/{}'.format(self.username)
		return self.getURI(uri, queryParams)
		
	#----------------------------------------
	# Query for a JSON response from the given URI
	def getURI(self, uri, queryParams):
		url = "{}/{}".format(self.baseUrl, uri)
		if queryParams:
			url = '{}?{}'.format(url, urllib.parse.urlencode(queryParams))
		return self.getURL(url)

	#----------------------------------------
	# Fetch a JSON response from the given URL
	# Intended for use with pagination when BB gives the complete 'next' URL
	def getURL(self, url):
		response = None
		try:
			headers = dict( [ self.authHeader ] )
			request = urllib.request.Request(url, headers=headers)
			response = urllib.request.urlopen(request)
			url = response.geturl() # may have changed in case of redirect
			code = response.getcode()
			log('getURL [{}] response code:{}'.format(url, code))
			content = response.read()
			content = content.decode('utf-8')  # Pre Python 3.6 hack (Raspberry Pi in 2019 still uses older release)
			return json.loads(content)
	
		except urllib.error.URLError as ex:
			log('getURL URLError:{} headers:\n{}'.format(str(ex), ex.headers), ex)
			
		except urllib.error.HTTPError as ex:
			log('getURL HTTPError:{} headers:\n{}'.format(str(ex), ex.headers), ex)
			
		except IOError as ex:
			log('getURL IOError:{} headers:\n{}'.format(str(ex), ex.headers), ex)
			
		finally:
			if (response != None):
				response.close()
		# ToDo: decode+return 40x error responses
		return {}


	#----------------------------------------
	# Create a new repository
	def createRepository(self, repository):
		uri = '2.0/repositories/{}/{}'.format(self.username, repository.get('slug'))
		return self.postURI(uri, repository)

	#----------------------------------------
	# POST a JSON request to the given URI
	def postURI(self, uri, postData):
		url = "{}/{}".format(self.baseUrl, uri)
		response = None
		try:
			encodedData = json.dumps(postData).encode('ascii')
			headers = dict( [ self.authHeader, self.jsonContentType ] )
			request = urllib.request.Request(url, data=encodedData, headers=headers, method='POST')
			response = urllib.request.urlopen(request)  # FIXME if this fails, still attempts to proceed with conversion of this repo. e.g. if create git repo fails
			url = response.geturl() # may have changed in case of redirect
			code = response.getcode()
			log('postURI [{}] response code:{}'.format(url, code))
			content = response.read()
			content = content.decode('utf-8')  # Pre Python 3.6 hack (Raspberry Pi in 2019 still uses older release)
			return json.loads(content)
	
		except urllib.error.URLError as ex:
			log('postURI URLError:{} url:{}, headers:\n{}'.format(str(ex), ex.url, ex.headers), ex)
			
		except urllib.error.HTTPError as ex:
			log('postURI HTTPError:{} headers:\n{}'.format(str(ex), ex.headers), ex)
			
		except IOError as ex:
			log('postURI IOError:{} headers:\n{}'.format(str(ex), ex.headers), ex)
			
		finally:
			if (response != None):
				response.close()
		# ToDo: decode+return 40x error responses
		return {}


#----------------------------------------
# Help with the repository migration
class RepositoryMigrationHelper:
	def __init__(self, workdir, repository, username, password):
		self.workdir = workdir
		self.repo = repository
		self.username = username
		self.password = password
	
	
	#----------------------------------------
	# Clone the Mercurial project using Git
	# Set the new Git repo remote
	# Push to the new Git repo
	def migrateHg2Git(self, gitRepo):
		hgUrl = self.findCloneUrl(self.repo)
		gitUrl = self.findCloneUrl(gitRepo)
		# if using https inject app password into URL
		if gitUrl.startswith('https'):
			gitUrl = gitUrl.replace('%s@bitbucket' % self.username, '%s:%s@bitbucket' % (self.username, self.password))
		repodir = self.repo['slug']+'.git'
		gitClone = 'git clone --mirror hg::{} {}'.format(hgUrl, repodir)
		gitPush = 'git push --mirror {}'.format(gitUrl)
		log('Migrating from hg::{} to git::{} ...'.format(hgUrl, gitUrl))
		print(gitClone)
		os.system(gitClone)
		with cwd(repodir):
			print(gitPush)
			os.system(gitPush)
		
	#----------------------------------------
	# Create an equivalent repository with -git suffix
	def buildGitEquivalent(self):
		gitRepo = {
			'scm': 'git',
			'slug': re.sub(r'(?i)(.+)(-hg)?$', r'\1-git', self.repo['slug']),
			'full_name': re.sub(r'(?i)(.+)(-hg)?$', r'\1-git', self.repo['full_name']),
			'name': re.sub(r'(?i)(.+)([ -]hg)?$', r'\1 (git)', self.repo['name']),
			'description': '{} (Git conversion)'.format(self.repo['description']),
			'is_private': self.repo['is_private'],
			'fork_policy': self.repo['fork_policy'],
			'language': self.repo['language'],
			'has_issues': self.repo['has_issues'],
			'has_wiki': self.repo.get('has_wiki'),
			'website': self.repo.get('website'),
			'links': {}
		}
		if 'mainbranch' in self.repo:
			gitRepo['mainbranch'] = self.repo['mainbranch']
		if 'avatar' in self.repo['links']:
			gitRepo['links']['avatar'] = self.repo['links']['avatar']
		if 'parent' in self.repo:
			gitRepo['parent'] = self.repo['parent']
		else:
			gitRepo['parent'] = {
				'type': 'repository',
				'name': self.repo['name'],
				'full_name': self.repo['full_name'],
				'uuid': self.repo['uuid']
			}
		return gitRepo
	
	#----------------------------------------
	def printRepository(self):
		cloneUrl = self.findCloneUrl(self.repo)
		pp = 'private' if repo.get('is_private') else 'public '
		print('{} {} {} URL:{} Name:{}'.format(pp, self.repo.get('scm'), self.repo.get('slug'), cloneUrl, self.repo.get('name')))
	
	#----------------------------------------
	# Find the clone URL of a repository, preferring 'https' protocol
	# as https can be used with application passwords, ssh requires keys
	@classmethod
	def findCloneUrl(cls, repo, preferred='https'):
		url = None
		for item in repo.get('links',{}).get('clone',{}):
			url = item['href']
			proto = item['name']
			if proto == preferred:
				break;
		return url
	
	#----------------------------------------
	# @return True if repo is Mercurial
	def isHgRepo(self):
		return self.repo.get('scm') == 'hg'		
	
@contextmanager
def cwd(newdir):
	prevdir = os.getcwd()
	os.chdir(os.path.expanduser(newdir))
	try:
		print('Working directory:',os.getcwd())
		yield
	finally:
		os.chdir(prevdir)
		print('Working directory:',os.getcwd())

def searchOsPath(name):
	found = None
	path = os.environ['PATH']
	for entry in path.split(os.pathsep):
		target = os.path.join(entry, name)
		if (os.path.isfile(target)):
			if (os.access(target, os.X_OK)):
				found = target
				break
	return found		
			

#----------------------------------------
# log message with a timestamp
def log(text, exception=None):
	sys.stderr.write( '[{}] {}\n'.format(datetime.datetime.utcnow().isoformat(), text) )
	if (exception != None):
		traceback.print_exc( file=sys.stderr )
		sys.stderr.write('\n')

#----------------------------------------
# Program options parsing and help
def parseArgs():
	parser = argparse.ArgumentParser(description='Migrate BitBucket Mercurial repositories to Git')
	options = parser.add_argument_group('mandatory arguments')
	options.add_argument('-u', '--user', required=True, help='BitBucket username')
	options.add_argument('-p', '--password', required=True, help='BitBucket password [App Password recommended - see https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html')
	options.add_argument('-w', '--workdir', default='work', help='Work directory for project conversions')
	args = parser.parse_args()
	return args
	
#----------------------------------------
# Main program start
# Approximate flow:
# Ensure git is configured and git-remote-hg is on PATH
# https://github.com/felipec/git-remote-hg 
# Connect (authenticate) to BitBucket API
# Read list of projects for user
# For each project:
#	If it's a Mercurial project:
#		If a Git equivalent exists already
#			skip
#		Create Git equivalent on BitBucket
#		Clone project using git clone --mirror using hg:: prefix
#		git push --mirror to new git project remote

args = parseArgs()

# Ensure work dir exists
workdir = Path(args.workdir)
if workdir.exists():
	if not workdir.is_dir():
		print('Workdir {} exists but is not a directory'.format(workdir))
		exit(1)
else:
	workdir.mkdir(parents=True)
	print('Created workdir: {}'.format(workdir))
os.chdir(str(workdir))
print('Using workdir: {}'.format(workdir))
	
# Check that tools have been installed
for tool in [ 'git', 'git-remote-hg' ]:
	search = searchOsPath(tool)
	if search != None:
		print('Found {} at {}'.format(tool, search))
	else:
		print('Missing {} - cannot convert to git'.format(tool))
		exit(2)

# Read list of all projects for user
bbApi = BBRestApi('https://api.bitbucket.org', username=args.user, secret=args.password)
repositories = bbApi.getAllRepositories({'fields':'-*.owner.*'})
repoLookup = { repo['slug']:repo for repo in repositories }

for repo in repositories:
	reponame = repo['full_name']
	helper = RepositoryMigrationHelper(workdir, repo, args.user, args.password)
	if not helper.isHgRepo():
		# Skip non-Mercurial repo
		log('Non-Hg repository - skipping {}'.format(reponame))
		continue
	#print('Existing repository:\n',json.dumps(repo, indent=3))
	print('Existing repository: {} {} {}'.format(repo['full_name'], repo['slug'], repo['name']))
	gitRepo = helper.buildGitEquivalent()
	#print('New Git repository:{}',json.dumps(gitRepo, indent=3))
	print('New Git repository: {} {} {}'.format(gitRepo['full_name'], gitRepo['slug'], gitRepo['name']))
	if gitRepo['slug'] in repoLookup:
		# Git conversion already done - skip it
		log('Conversion already done - skipping {}'.format(reponame))
		continue
	log('Creating new Git repository:{}'.format(gitRepo.get('slug')))
	status = bbApi.createRepository(gitRepo)
	helper.migrateHg2Git(status)
	log('Repository {} done.\n'.format(reponame))

log('Done.')

